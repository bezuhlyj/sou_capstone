#include <OneWire.h>

int sensor1 = 2; //DS18S20 Signal pin on digital 2
int sensor2 = 3; //DS18S20 Signal pin on digital 3
int heatPin = 8; //relay for heater
int coolPin = 9; //relay for cooler

OneWire ds(sensor1);
OneWire ds2(sensor2);

void setup(void) {
  pinMode(heatPin, OUTPUT);
  pinMode(coolPin, OUTPUT);
  Serial.begin(9600);
}

void loop(void) {

    if (getAvg() < 45) {
      do {
        digitalWrite(heatPin, HIGH);
        Serial.println(getAvg());
        delay(1000);
      } while (getAvg() < 45);
      digitalWrite(heatPin, LOW);
    }
    else if (getAvg() > 51) {
      do {
        digitalWrite(coolPin, HIGH);
        Serial.println(getAvg());
        delay(1000);
      } while (getAvg() > 51);
      digitalWrite(coolPin, LOW);
    }
    
  Serial.println(getAvg());
  
  delay(2000); //just here to slow down the output so it is easier to read
  
}

float getAvg(){
  
  float temp1 = getTemp1();
  float temp2 = getTemp2();
  
  return ((temp1 + temp2) / 2);
}

float getTemp1(){
  //returns the temperature from one DS18S20

  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      //no more sensors on chain, reset search
      ds.reset_search();
      return -1000;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return -1000;
  }

  if ( addr[0] != 0x10 && addr[0] != 0x28) {
      Serial.print("Device is not recognized");
      return -1000;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); // Read Scratchpad

  
  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }
  
  ds.reset_search();
  
  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TempCelsius = tempRead / 16;
  
  return (((TempCelsius * 9)/5) + 32);
  
}

float getTemp2(){
  //returns the temperature from one DS18S20

  byte data2[12];
  byte addr2[8];

  if ( !ds2.search(addr2)) {
      //no more sensors on chain, reset search
      ds2.reset_search();
      return -1000;
  }

  if ( OneWire::crc8( addr2, 7) != addr2[7]) {
      Serial.println("CRC is not valid!");
      return -1000;
  }

  if ( addr2[0] != 0x10 && addr2[0] != 0x28) {
      Serial.print("Device is not recognized");
      return -1000;
  }

  ds2.reset();
  ds2.select(addr2);
  ds2.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds2.reset();
  ds2.select(addr2);    
  ds2.write(0xBE); // Read Scratchpad

  
  for (int j = 0; j < 9; j++) { // we need 9 bytes
    data2[j] = ds2.read();
  }
  
  ds2.reset_search();
  
  byte MSB2 = data2[1];
  byte LSB2 = data2[0];

  float tempRead2 = ((MSB2 << 8) | LSB2); //using two's compliment
  float TempCelsius2 = tempRead2 / 16;
  
  return (((TempCelsius2 * 9)/5) + 32);
  
}


