#!/usr/bin/python
import mysql.connector

#TODO: use variables to be passed in through interface(TBD)
highTemp = 127
lowTemp = 58

#create connection, set database
cnx = mysql.connector.connect(user='root', password='password', host='127.0.0.1', database='strawMan')
cursor = cnx.cursor()

#TODO: format query to use highTemp and lowTemp variables
# create query and execute
query = ("INSERT INTO beer ( hTemp, lTemp) VALUES (99, 98)")
cursor.execute(query)


cnx.commit()
cursor.close()
cnx.close()
