#!/usr/bin/python
import mysql.connector

#create connection and set database
cnx = mysql.connector.connect(user='root', password='password', host='127.0.0.1', database='strawMan')

cursor = cnx.cursor()

#create read-query, execute, and loop through and print results
query = ("SELECT hTemp, lTemp FROM beer;")
cursor.execute(query)
for (hTemp, lTemp) in cursor:
	print("high temp: {} low temp: {}".format(hTemp, lTemp))
cursor.close()

cnx.close()
