<?php
include'model.php';
include'config.php';
require'php_serial.class.php';

class Beer{

public $id, $title, $hiTemp, $lowTemp, $notes;
	
	function __construct($id, $title, $hiTemp, $lowTemp, $notes){
		$this->id = $id;
		$this->title = $title;
		$this->lowTemp = $lowTemp;
		$this->hiTemp = $hiTemp;
		$this->notes = $notes;
		}
}

class BeerModel extends Model{

	function readTemp(){
		$serial = new phpSerial();
		$serial->deviceSet("/dev/ttyUSB0");
		$serial->confBaudRate(9600);
		$serial->confParity("none");
		$serial->confCharacterLength(8);
		$serial->confStopBits(1); 
		$serial->confFlowControl("none");
		$serial->deviceOpen();
		$read = $serial->readPort();
		return $read;
	}

	function insert($beer) {
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (mysqli_connect_errno()) {
    			echo "unable to connect, please try again \r\n";
				echo DB_HOST;
				die;
            }
		$query = $conn->prepare("INSERT INTO beer (title, hiTemp, lowTemp, notes) VALUES (?, ?, ?, ?)");
		echo $conn->error;
		$query->bind_param("siis", $_POST["title"], $_POST["hiTemp"], $_POST["lowTemp"], $_POST["notes"]);		
		$query->execute();
		mysqli_close($conn);
	}
	function delete($beer) {
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (mysqli_connect_errno()) {
    			echo "unable to connect, please try again \r\n";
				echo DB_HOST;
				die;
            }
		$query = $conn->prepare("DELETE FROM beer WHERE id = ? ");
		$query->bind_param("i", $id);
		$query->execute();		
		mysqli_close($conn);
	}

	function findAll(){
		$dummyData = array(
                    new Beer("0", "Ale", "71", "65", "Make it good."),
                    new Beer("1", "Lager", "51", "45", "Make it good."),
                    new Beer("2", "Cold Crash", "38", "34", "Kill that yeast."),
                 );
/* 		$recipes = array();
		$query = "SELECT * from beer";
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if (mysqli_connect_errno()) {
    			echo "unable to connect, please try again \r\n";
				echo DB_HOST;
				die;
            }
		$res = $conn->query($query);
			while ($row = $res->fetch_assoc()) {
				$results[] = new Beer(
						$row['id'],
						$row['title'],
						$row['hiTemp'],
						$row['lowTemp']
						);	
			}
		mysqli_close($conn); 
		return $results;
		$res->free(); */
		return $dummyData;
	}
}
