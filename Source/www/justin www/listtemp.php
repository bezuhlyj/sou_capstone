<div><a href="index.php?action=form">Insert a recipe</a></div>
<div><a href="index.php?action=listtemp">View current Temp</a></div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Title</th>                  
            <th>High Temp</th>
            <th>Low Temp</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
    <?php 
		foreach ($this->data as $recipe) { ?>     
        <tr>
            <td><?php echo htmlentities($recipe->id); ?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>                                
            <td><?php echo htmlentities($recipe->hiTemp); ?></td>
            <td><?php echo htmlentities($recipe->lowTemp); ?></td>
            <td><?php echo htmlentities($recipe->notes); ?></td>
        </tr>                                
    <?php } ?>          
    </tbody>                
</table>     