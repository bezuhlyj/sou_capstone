<?php
class View {
	function __construct($view,$data) {
		$this->view = $view;
		$this->data = $data;
		}
		
	function render() {
    ob_start();
    include $this->view . '.php';
    $content = ob_get_clean();
	include 'layout.php';
	}
	
	
}