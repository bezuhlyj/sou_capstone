	<form action="index.php?action=form" method="post">
		<fieldset>
			<legend>Insert a Beer Recipe</legend>
			<label>Title</label>
				<input class="input-xxlarge" type="text" placeholder="Beer Name" name="title">
			<label>Hi Temp</label>
				<div><input class="input-xlarge" type="text" placeholder="High Temp" name="hiTemp"></div>
			<label>Low Temp</label>
				<div><input class="input-xlarge" type="text" placeholder="Low Temp" name="lowTemp"></div>
	
			<label>Notes</label>
			<div><textarea name="notes" class="input-block-level" rows="5"></textarea></div>		    		
								
			<button type="submit" class="btn btn-primary">Submit</button>
		</fieldset>
	</form>
        <div><a href="index.php">Return to recipe list</a></div>