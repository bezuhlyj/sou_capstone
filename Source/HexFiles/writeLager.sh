#!/bin/bash
clear
echo "Programming the Arduino with Lager Recipe"
echo
echo "Backing up current EEPROM, in case of an error"
echo
avrdude -v -p atmega328p -c arduino -P /dev/ttyACM0 -b 115200 -Uflash:r:flash_backup.hex:i
echo "DONE"
echo
echo "Writing ale.cpp.hex"
avrdude -v -p atmega328p -c arduino -P /dev/ttyACM0 -b 115200 -Uflash:w:lager.cpp.hex:i

