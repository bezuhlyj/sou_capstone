#!/bin/bash
# raspberryBuildScript
# this script exists in the /Source directory
# Jeremy French 2/14/2013

# This is the second build script in a two part series
# this script assumes that the necessary packages are installed and running (httpd, mysqld, python, etc.) and that a previous version of the repo is backed up in /var/git/backup/<repoName>.


# Clone the current repo version
cd /var/git/current/sou_capstone
sudo git pull
# copy www files to apache www dir
cp /var/git/current/sou_capstone/Source/www/* /var/www/

# chmod 755 all python scripts to be called by apache user
sudo chmod 755 /var/git/current/sou_capstone/Source/script/*
